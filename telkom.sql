-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2018 at 05:40 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telkom`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_dunning`
--

CREATE TABLE `data_dunning` (
  `WITEL` varchar(255) NOT NULL,
  `DATEL` varchar(255) NOT NULL,
  `CMDF` varchar(255) NOT NULL,
  `ND` varchar(255) NOT NULL,
  `ND_REFERENCE` varchar(255) NOT NULL,
  `TGL_AKTIF` varchar(255) NOT NULL,
  `UMUR` varchar(255) NOT NULL,
  `RANGE` varchar(255) NOT NULL,
  `DP` varchar(255) NOT NULL,
  `KWADRAN` varchar(255) NOT NULL,
  `TUNDA_CABUT` varchar(255) NOT NULL,
  `BLNTAG_TUNDA_CABUT` varchar(255) NOT NULL,
  `CITEM` varchar(255) NOT NULL,
  `DESC_ITEM` varchar(255) NOT NULL,
  `TAG_INET` varchar(255) NOT NULL,
  `TAG_POTS` varchar(255) NOT NULL,
  `NOM_RESSOURCE` varchar(255) NOT NULL,
  `ONU_STATUS` varchar(255) NOT NULL,
  `ABO-INET` varchar(255) NOT NULL,
  `PAKET` varchar(255) NOT NULL,
  `TYPE_FILE` varchar(255) NOT NULL,
  `STATUS` varchar(255) NOT NULL,
  `KETERANGAN` varchar(255) NOT NULL,
  `POSISI` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_upload`
--

CREATE TABLE `data_upload` (
  `id` int(11) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `upload_at` datetime NOT NULL,
  `upload_by` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `name`, `deskripsi`) VALUES
(1, 'aso', 'ASO'),
(2, 'ta', 'Telkom Akses'),
(3, 'ps', 'Personal Service'),
(4, 'hs', 'Home Service'),
(5, 'cc', 'Customer Care'),
(6, 'bl', 'Billing');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', 'admin', 'admin@email.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_upload`
--
ALTER TABLE `data_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_upload`
--
ALTER TABLE `data_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
