<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>VIEW DATA</h2>
        </div> -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            VIEW DATA UNIT ASO
                        </h2>
                    </div>
                    <div class="body">
                        <div>
                            <table border="0" align="center">
                                <tr>
                                    <td>
                                        <span>WITEL : </span>
                                        <select id="witel">
                                            <option value="kosong" <?php if($sel_witel == "kosong") echo "selected";?>>ALL</option>
                                            <?php foreach($witel as $key){?>
                                            <option value="<?= $key->witel?>" <?php if($sel_witel == $key->witel) echo "selected";?>><?= strtoupper($key->witel)?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <span>DATEL : </span>
                                        <select id="datel">
                                            <option value="kosong" <?php if($sel_datel == "kosong") echo "selected";?>>ALL</option>
                                            <?php foreach($datel as $key){?>
                                            <option value="<?= $key->datel?>" <?php if($sel_datel == $key->datel) echo "selected";?>><?= strtoupper($key->datel)?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <button id="filter" type="button">FILTER</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>WITEL</th>
                                        <th>DATEL</th>
                                        <th>CMDF</th>
                                        <th>ND</th>
                                        <th>ND_REFERENCE</th>
                                        <th>TGL_AKTIF</th>
                                        <th>UMUR</th>
                                        <th>RANGE</th>
                                        <th>DP</th>
                                        <th>KWADRAN</th>
                                        <th>TUNDA_CABUT</th>
                                        <th>BLNTAG_TUNDA_CABUT</th>
                                        <th>CITEM</th>
                                        <th>DESC_ITEM</th>
                                        <th>TAG_INET</th>
                                        <th>TAG_POTS</th>
                                        <th>NOM_RESSOURCE</th>
                                        <th>ONU_STATUS</th>
                                        <th>ABO-INET</th>
                                        <th>PAKET</th>
                                        <th>POSISI DATA</th>
                                        <th>KETERANGAN</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- For Material Design Colors -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="judul_modal">Modal title</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="nd" id="nd_edit">
                <input type="hidden" name="type" id="type_edit">

                <span>POSISI UNIT DATA</span>
                &nbsp&nbsp
                <select name="posisi" id="posisi_edit">
                    <option value="1">ASO</option>
                    <option value="2">Telkom Akses</option>
                    <option value="3">Personal Service</option>
                    <option value="4">Home Service</option>
                    <option value="5">Customer Care</option>
                </select>
                <br><br>
                <span>KETERANGAN</span>
                &nbsp&nbsp
                <textarea rows="5" name="keterangan" id="keterangan_edit" class="form-control no-resize" placeholder="Tambahkan keterangan disini"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect btn_save">SAVE CHANGES</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        var witel = $('#witel option:selected').val()
        var datel = $('#datel option:selected').val()

        var table = $('.js-exportable').DataTable({
            serverSide: true,
            processing: true,
            language : {
              processing : '<span style="width:100%;"><img src="http://www.snacklocal.com/images/ajaxload.gif"></span>'
            },
            ajax: {
                url: "<?=base_url()?>index.php/unit/api_view/1",
                data: {
                    witel: witel,
                    datel: datel
                }
            },
            columns: [            
                {data: 'witel', name: 'witel', orderable: false},
                {data: 'datel', name: 'datel', orderable: false},
                {data: 'cmdf', name: 'cmdf', orderable: false},
                {data: 'nd', name: 'nd'},
                {data: 'nd_reference', name: 'nd_reference'},
                {data: 'tgl_aktif', name: 'tgl_aktif'},
                {data: 'umur', name: 'umur'},
                {data: 'range', name: 'range'},
                {data: 'dp', name: 'dp', orderable: false},
                {data: 'kwadran', name: 'kwadran'},
                {data: 'tunda_cabut', name: 'tunda_cabut'},
                {data: 'blntag_tunda_cabut', name: 'blntag_tunda_cabut'},
                {data: 'citem', name: 'citem', orderable: false},
                {data: 'desc_item', name: 'desc_item', orderable: false},
                {data: 'tag_inet', name: 'tag_inet', orderable: false},
                {data: 'tag_pots', name: 'tag_pots', orderable: false},
                {data: 'nom_ressource', name: 'nom_ressource', orderable: false},
                {data: 'onu_status', name: 'onu_status', orderable: false},
                {data: 'abo_inet', name: 'abo_inet'},
                {data: 'paket', name: 'paket', orderable: false},
                {data: 'posisi', name: 'posisi'},
                {data: 'keterangan', name: 'keterangan'},
                {data: 'act', name: 'act'}
            ]
        });

        $('#filter').on("click",function(){
            var witel = $('#witel option:selected').val()
            var datel = $('#datel option:selected').val()
            window.location.href = "<?= base_url()?>index.php/unit/aso?w="+witel+"&d="+datel+"";
        });

        table.on('click', 'button.btn_edit', function () {
            var nd = $(this).data("x");
            var type = $(this).data("y");

            $('#nd_edit').val(nd);
            $('#type_edit').val(type);
            
            $('#judul_modal').text("Edit Data ND : "+nd);

            $('#keterangan_edit').val("");
            $('#myModal').modal("show");
        });

        $('.btn_save').on("click",function(){
            var nd_edit = $('#nd_edit').val();
            var type_edit = $('#type_edit').val();
            var posisi_edit = $('#posisi_edit').val();
            var keterangan_edit = $('#keterangan_edit').val();

            $.ajax({
                url: "<?= base_url('index.php/unit/update_data')?>",
                type: 'POST',
                data: {
                    nd:nd_edit,
                    type:type_edit,
                    posisi:posisi_edit,
                    keterangan:keterangan_edit
                },
                success: function(data){
                    if(data){
                        $('#myModal').modal("hide");
                        $.notify("Sukses Update Data", "success");
                        table.ajax.reload();
                    }else{
                        $('#myModal').modal("hide");
                        $.notify("Gagal Update Data", "error");
                        table.ajax.reload();
                    }
                },
            });
        });
    });

</script>