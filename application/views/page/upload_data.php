<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>UPLOAD DATA</h2>
        </div> -->
        <!-- Color Pickers -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            UPLOAD DATA EXCEL
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <form id="form_upload" method="post" enctype="multipart/form-data">
                                <table border="0" align="center" id="tb_upload">
                                    <tr>
                                        <td>
                                            <input type="file" name="data" id="data">
                                        </td>
                                        <!-- <td>
                                            <button id="btn_upload" type="button">UPLOAD DATA</button>
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Download Example File Upload : <a href="<?= base_url()?>assets/example.xlsx">EXAMPLE_FILE_UPLOAD.XLSX</a></td>
                                    </tr>
                                </table>
                                <h1 id="jd_progres" align="center">Processing...</h1>
                                </form>
                                <div id="progres">
                                    <div class="progress-bar bg-orange progress-bar-striped active" role="progressbar">
                                        <p id="persen">0%</p>
                                    </div>
                                    <br>
                                    <center>TOTAL DATA : <span id="total">0</span> &nbsp&nbsp TOTAL DATA UPLOADED : <span id="done">0</span></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Color Pickers -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            FILE EXCEL UPLOADED
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Nama File</th>
                                        <th>Uploaded At</th>
                                        <th>Uploaded By</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $("#progres").hide();
    $("#jd_progres").hide();

    $(document).ready(function(){

        var table = $('.js-exportable').DataTable({
            serverSide: true,
            processing: true,
            language : {
              processing : '<span style="width:100%;"><img src="http://www.snacklocal.com/images/ajaxload.gif"></span>'
            },  
            ajax: '<?=base_url()?>index.php/data/api_file',
            columns: [            
                {data: 'nama_file', name: 'nama_file'},
                {data: 'upload_at', name: 'upload_at'},
                {data: 'upload_by', name: 'upload_by'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false}
            ]
        });

        $("#data").on("change", function (event) {
            swal({
                title: "WARNING!",
                text: "Jangan reload atau refresh halaman ini, sistem sedang melakukan proses upload data!",
                icon: "warning",
                buttons: false
            });

            var url = URL.createObjectURL(event.target.files[0]);
            var oReq = new XMLHttpRequest();
            oReq.open("GET", url, true);
            oReq.responseType = "arraybuffer";

            oReq.onload = function(e) {
                var arraybuffer = oReq.response;

                /* convert data to binary string */
                var data = new Uint8Array(arraybuffer);
                var arr = new Array();
                for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");
                /* Call XLSX */
                var workbook = XLSX.read(bstr, {type:"binary", cellDates:true, cellStyles:true});

                /* DO SOMETHING WITH workbook HERE */
                var first_sheet_name = workbook.SheetNames[0];
                /* Get worksheet */
                var worksheet = workbook.Sheets[first_sheet_name];
                var dataJSON = XLSX.utils.sheet_to_json(worksheet,{raw:true});
                //upload(dataJSON);
                upload_file($('#form_upload')[0],dataJSON);
            }

            oReq.send();
        });

        function upload_file(data,data2){
            var formData = new FormData(data);
          
            $.ajax({
              url: "<?php echo base_url('index.php/data/file_upload')?>",
              type: 'POST',
              data: formData,
              async: false,
              cache: false,
              contentType: false,
              enctype: 'multipart/form-data',
              processData: false,
              success: function (response) {
                var resp = JSON.parse(response);
                if(resp.status){
                    swal({
                        title: "INFO !",
                        text: resp.message,
                        icon: "success"
                    }).then(function() {
                      upload_data(data2,resp.id);
                    });
                }else{
                    swal({
                        title: "WARNING !",
                        text: resp.message,
                        icon: "warning"
                    }).then(function() {
                      location.reload();
                    });
                }
              }
            });
        }

        function upload_data(data,file_id){
            swal.close();
            $('#progres').show();
            $('#jd_progres').show();
            $('#tb_upload').hide();
            $('#total').text(data.length);
            var jumlah_data = data.length;
            var start = 0;
            var loop = 10000;
            var jum = 0
            var x = 0;

            var i = 1;
            var data_arr = new Array();
            while(i > 0){
                jum = start + loop;
                if(jum <= jumlah_data){
                    for(a = x; a < jum; a++){
                        data_arr.push(data[a]);
                    }
                    proses(data_arr,file_id);
                    data_arr = new Array();

                    start += loop;
                    x = jum;
                    i++;
                }else{
                    loop = (start + loop) - jumlah_data;
                    for(a = x; a < jumlah_data; a++){
                        data_arr.push(data[a]);
                    }
                    proses(data_arr,file_id);
                    data_arr = new Array();
                    i = -1;
                }
            }
        }

        function proses(item,file_id){
            var dataString = JSON.stringify(item);
            $.ajax({
                url: "<?= base_url('index.php/data/proses_upload')?>",
                type: 'POST',
                data: {myData:dataString,file_id:file_id},
                dataType: "json",
                success: function(data){
                    var sukses = parseInt($('#done').text());
                    var total = parseInt($('#total').text());

                    sukses += data;
                    var persen = sukses/total*100;
                    persen = Math.round(persen);
                    $('.progress-bar').css("width", persen+"%");
                    $('.progress-bar').css("height", "20");
                    $('#persen').text(persen+"%");
                    $('#done').text(sukses);
                },
                complete: function() {
                    var aa = parseInt($('#total').text());
                    var bb = parseInt($('#done').text());
                    if(aa == bb){
                        swal({
                            title: "INFO !",
                            text: "Upload data sukses, silahkan menuju menu UNIT ASO untuk melihat data yang sudah terupload!",
                            icon: "success"
                        }).then(function(){
                            $('#jd_progres').text("DONE UPLOAD");
                            update_file(file_id,"sukses");
                            table.ajax.reload();
                        });
                    }else{
                        update_file(file_id,"gagal");
                        table.ajax.reload();
                    }
                }
            });
        }

        function update_file(file_id,status) {
            $.ajax({
                url: "<?= base_url('index.php/data/update_file')?>",
                type: 'POST',
                data: {
                    file_id:file_id,
                    status:status
                }
            });
        }

    });

</script>