<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

        
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">TIDAK ADA STATUS</div>
                        <div class="number count-to" data-from="0" data-speed="1000" data-fresh-interval="20"><?= $kat0?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">KATEGORI 1</div>
                        <div class="number count-to" data-from="0" data-speed="15" data-fresh-interval="20"><?= $kat1?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">KATEGORI 2</div>
                        <div class="number count-to" data-from="0" data-speed="1000" data-fresh-interval="20"><?= $kat2?></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row clearfix">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2>TASK KATEGORI 1</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Total Data</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="label bg-light-blue">NULL</span></td>
                                        <td><?= $kat1_null?></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label bg-green">DONE</span></td>
                                        <td><?= $kat1_done?></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label bg-blue">PROSES</span></td>
                                        <td><?= $kat1_proses?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2>TASK KATEGORI 2</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Total Data</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="label bg-light-blue">NULL</span></td>
                                        <td><?= $kat2_null?></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label bg-green">DONE</span></td>
                                        <td><?= $kat2_done?></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label bg-blue">PROSES</span></td>
                                        <td><?= $kat2_proses?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>