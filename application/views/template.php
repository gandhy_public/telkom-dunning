<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Dashboard</title>
    <!-- Favicon-->
    <link rel="icon" href="<?= base_url()?>assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?= base_url()?>assets/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="<?= base_url()?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="<?= base_url()?>assets/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="<?= base_url()?>assets/plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="<?= base_url()?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?= base_url()?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?= base_url()?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="<?= base_url()?>assets/plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= base_url()?>assets/css/themes/all-themes.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?= base_url()?>assets/index.html">DASHBOARD DATA DUNNING</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?= base_url()?>assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $user_session['username']?></div>
                    <div class="email"><?= $user_session['email']?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?= base_url()?>index.php/auth/logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <?php
                $segment1 = $this->uri->segment(1);
                $segment2 = $this->uri->segment(2);
                $aktif1 = "";
                $aktif2 = "";
                $aktif3 = "";
                $aktif4 = "";
                $aktif5 = "";
                $aktif6 = "";
                $aktif7 = "";
                $aktif8 = "";
                ?>
                <ul class="list">
                    <li class="header">MENU NAVIGATION</li>
                    <li <?= "class='active'";/*$aktif1*/ ?>>
                        <a href="<?= base_url()?>">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>

                    <li <?= $aktif2 ?>>
                        <a href="<?= base_url()?>index.php/data/upload">
                            <i class="material-icons">backup</i>
                            <span>Upload Data</span>
                        </a>
                    </li>
                    <li <?= $aktif3 ?>>
                        <a href="<?= base_url()?>index.php/unit/aso?w=kosong&d=kosong&s=">
                            <i class="material-icons">view_list</i>
                            <span>Unit ASO</span>
                        </a>
                    </li>
                    <li <?= $aktif4 ?>>
                        <a href="<?= base_url()?>index.php/unit/ta?w=kosong&d=kosong&s=">
                            <i class="material-icons">view_list</i>
                            <span>Unit Telkom Akses</span>
                        </a>
                    </li>
                    <li <?= $aktif5 ?>>
                        <a href="<?= base_url()?>index.php/unit/ps?w=kosong&d=kosong&s=">
                            <i class="material-icons">view_list</i>
                            <span>Unit Personal Service</span>
                        </a>
                    </li>
                    <li <?= $aktif6 ?>>
                        <a href="<?= base_url()?>index.php/unit/hs?w=kosong&d=kosong&s=">
                            <i class="material-icons">view_list</i>
                            <span>Unit Home Service</span>
                        </a>
                    </li>
                    <li <?= $aktif7 ?>>
                        <a href="<?= base_url()?>index.php/unit/cc?w=kosong&d=kosong&s=">
                            <i class="material-icons">view_list</i>
                            <span>Unit Customer Care</span>
                        </a>
                    </li>
                    <li <?= $aktif8 ?>>
                        <a href="<?= base_url()?>index.php/unit/bl?w=kosong&d=kosong&s=">
                            <i class="material-icons">view_list</i>
                            <span>Unit Billing</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">Telkom Indonesia</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= base_url()?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Bootstrap Colorpicker Js -->
    <script src="<?= base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- noUISlider Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/nouislider/nouislider.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/raphael/raphael.min.js"></script>
    <script src="<?= base_url()?>assets/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<?= base_url()?>assets/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?= base_url()?>assets/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?= base_url()?>assets/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?= base_url()?>assets/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?= base_url()?>assets/plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Editable Table Plugin Js -->
    <script src="<?= base_url()?>assets/plugins/editable-table/mindmup-editabletable.js"></script>

    <!-- Custom Js -->
    <script src="<?= base_url()?>assets/js/admin.js"></script>
    <script src="<?= base_url()?>assets/xlsx.full.min.js"></script>
    <script src="<?= base_url()?>assets/notify.min.js"></script>
    <script src="<?= base_url()?>assets/notify.js"></script>
    <!-- <script src="<?= base_url()?>assets/js/pages/index.js"></script> -->

    <!-- Demo Js -->
    <!-- <script src="<?= base_url()?>assets/js/demo.js"></script> -->

    <?php $this->load->view($content)?>
</body>

</html>
