<?php
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
date_default_timezone_set("Asia/Jakarta");

class Unit extends CI_Controller
{
	private $user_session = null;

	public function __construct(){
		parent::__construct();
		$this->user_session = $this->session->userdata('user_session');
		$this->load->model('M_unit');
		if(!$this->user_session){
			redirect('auth');
		}
	}

	public function aso()
	{
		$data['user_session'] = $this->user_session;
		$data['content'] = 'page/unit/aso';

		$data['witel'] = $this->M_unit->getWitel();
		$data['datel'] = $this->M_unit->getDatel();

		$data['sel_witel'] = $_GET['w'];
		$data['sel_datel'] = $_GET['d'];

		$this->load->view('template',$data);
	}

	public function api_view($posisi)
	{
		$lastID = $this->M_unit->getLastID();
		$data_where = [
			'witel' => ($_REQUEST['witel'] == "kosong") ? "" : $_REQUEST['witel'],
			'datel' => ($_REQUEST['datel'] == "kosong") ? "" : $_REQUEST['datel'],
			'id' => $lastID,
			'posisi' => $posisi,
		];
		$list = $this->M_unit->get_datatables('aso',$data_where);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $key) {        	
            $no++;
            $row = array();

            $row['witel'] 				= $key->WITEL;
            $row['datel'] 				= $key->DATEL;
            $row['cmdf'] 				= $key->CMDF;
            $row['nd'] 					= $key->ND;
            $row['nd_reference'] 		= $key->ND_REFERENCE;
            $row['tgl_aktif'] 			= $key->TGL_AKTIF;
            $row['umur'] 				= $key->UMUR;
            $row['range'] 				= $key->RANGE;
            $row['dp'] 					= $key->DP;
            $row['kwadran'] 			= $key->KWADRAN;
            $row['tunda_cabut'] 		= $key->TUNDA_CABUT;
            $row['blntag_tunda_cabut'] 	= $key->BLNTAG_TUNDA_CABUT;
            $row['citem'] 				= $key->CITEM;
            $row['desc_item'] 			= $key->DESC_ITEM;
            $row['tag_inet'] 			= $key->TAG_INET;
            $row['tag_pots'] 			= $key->TAG_POTS;
            $row['nom_ressource'] 		= $key->NOM_RESSOURCE;
            $row['onu_status'] 			= $key->ONU_STATUS;
            $row['abo_inet'] 			= $key->ABO;
            $row['paket'] 				= $key->PAKET;
            $row['posisi'] 				= $key->POSISI;
            $row['keterangan'] 				= $key->KETERANGAN;
            $row['act'] 				= "<center><button data-x='".$key->ND."' data-y='".$key->TYPE_FILE."' class='btn_edit' type='button'>EDIT</button></center>";
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->M_unit->count_all('aso',$data_where),
            "recordsFiltered" => $this->M_unit->count_filtered('aso',$data_where),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function update_data()
	{
		$nd = $this->input->post('nd');
		$type = $this->input->post('type');
		$posisi = $this->input->post('posisi');
		$keterangan = $this->input->post('keterangan');

		$update_data = $this->M_unit->updateData($posisi,$keterangan,$nd,$type);
		if($update_data){
			echo TRUE;
		}else{
			echo FALSE;
		}
	}

}
?>