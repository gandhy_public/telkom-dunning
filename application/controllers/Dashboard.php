<?php
class Dashboard extends CI_Controller
{
	private $user_session = null;

	public function __construct(){
		parent::__construct();
		$this->user_session = $this->session->userdata('user_session');
		$this->load->model('M_data');
		if(!$this->user_session){
			redirect('auth');
		}
	}

	public function index()
	{
		$data['user_session'] = $this->user_session;
		$data['content'] = 'page/dashboard';

		$lastID = $this->M_data->getLastID();
		if($lastID){
			$data['kat0'] = $this->M_data->getCountData('tidak ada status',$lastID);
			$data['kat1'] = $this->M_data->getCountData('kategori 1',$lastID);
			$data['kat2'] = $this->M_data->getCountData('kategori 2',$lastID);

			$data['kat1_null'] = $this->M_data->getCountDataKategori('kategori 1',$lastID,"kosong");
			$data['kat1_done'] = $this->M_data->getCountDataKategori('kategori 1',$lastID,"done");
			$data['kat1_proses'] = $this->M_data->getCountDataKategori('kategori 1',$lastID,"proses");

			$data['kat2_null'] = $this->M_data->getCountDataKategori('kategori 2',$lastID,"kosong");
			$data['kat2_done'] = $this->M_data->getCountDataKategori('kategori 2',$lastID,"done");
			$data['kat2_proses'] = $this->M_data->getCountDataKategori('kategori 2',$lastID,"proses");
		}else{
			$data['kat0'] = "0";
			$data['kat1'] = "0";
			$data['kat2'] = "0";

			$data['kat1_null'] = "0";
			$data['kat1_done'] = "0";
			$data['kat1_proses'] = "0";

			$data['kat2_null'] = "0";
			$data['kat2_done'] = "0";
			$data['kat2_proses'] = "0";
		}

		$this->load->view('template',$data);
	}
}
?>