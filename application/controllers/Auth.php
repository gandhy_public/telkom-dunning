<?php
class Auth extends CI_Controller
{
	private $user_session = null;

	public function __construct(){
		parent::__construct();
		$this->load->model('M_auth');
		$this->user_session = $this->session->userdata('user_session');
	}
	
	function index()
	{
		if(!$this->user_session){
			$this->load->view('login');
		}else{
			redirect('dashboard');
		}
	}

	public function login()
	{
		if(!$this->user_session){
			try {
				$username = $this->input->post('username');
				$password = $this->input->post('password');	

				if(empty($username)) throw new Exception("Username is required", 1);
				if(empty($password)) throw new Exception("Password is required", 1);
				
				$getUser = $this->M_auth->getUser($username,$password);
				if(!empty($getUser)){
					$data_session = [
						'username' => $getUser[0]->username,
						'password' => $getUser[0]->password,
						'email' => $getUser[0]->email,
					];
					$this->session->set_userdata('user_session',$data_session);
					redirect('dashboard');
				}else{
					throw new Exception("Data user not found", 1);
				}
			} catch (Exception $e) {
				$this->session->set_flashdata('notif_auth',$e->getMessage());
				redirect('auth');
			}
		}else{
			redirect('dashboard');
		}
	}

	public function logout()
	{
		if($this->user_session){
			$this->session->unset_userdata('user_session');
			redirect('auth');
		}else{
			redirect('auth');
		}
	}
}
?>