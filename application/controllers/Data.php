<?php
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
date_default_timezone_set("Asia/Jakarta");

class Data extends CI_Controller
{
	private $user_session = null;

	public function __construct(){
		parent::__construct();
		$this->user_session = $this->session->userdata('user_session');
		$this->load->model('M_data');
		if(!$this->user_session){
			redirect('auth');
		}
	}

	public function upload()
	{
		$data['user_session'] = $this->user_session;
		$data['content'] = 'page/upload_data';
		$this->load->view('template',$data);
	}

	public function api_file()
	{
		$list = $this->M_data->get_datatables('file');
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $key) {        	
            $no++;
            $row = array();

            if($key->status == "sukses"){
            	$status = "<center><span class='label label-success'>Sukses</span></center>";
            	$action = "";
            }elseif($key->status == "pending"){
            	$status = "<center><span class='label label-warning'>Pending</span></center>";
            	$action = "<center><a href='javascript:window.location.reload(true)'>Reupload</a></center>";
            }else{
            	$status = "<center><span class='label label-danger'>Gagal</span></center>";
            	$action = "<center><a href='javascript:window.location.reload(true)'>Reupload</a></center>";
            }

            $row['nama_file'] 	= "<a href='".base_url()."assets/upload/".$key->nama_file."' target='_blank'>".$key->nama_file."</a>";
            $row['upload_at'] 	= $key->upload_at;
            $row['upload_by'] 	= $key->upload_by;
            $row['status'] 		= $status;
            $row['action'] 		= $action;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->M_data->count_all("file"),
            "recordsFiltered" => $this->M_data->count_filtered("file"),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
	}

	public function file_upload()
	{
		try {
			$file_type = explode(".", $_FILES['data']['name']);
			$file_type = strtolower($file_type[1]);
			if($file_type != "xlsx") throw new Exception("Type file harus .XLSX", 1);

			$fileName   = date('Y-m-d&H-i-s').'_'.$_FILES['data']['name'];
	        $fileName   = str_replace(" ", "_", $fileName);
	        $destination_folder = 'assets/upload/';
	        $file = "$destination_folder/$fileName";

			$upload_file = move_uploaded_file($_FILES['data']['tmp_name'], $file);
			if(!$upload_file) throw new Exception("Gagal upload file", 1);
			
			$data = [
				'nama_file' => $fileName,
				'upload_at' => date('Y-m-d H:i:s'),
				'upload_by' => $this->user_session['username'],
				'status' => "pending"
			];
			$insertFile = $this->M_data->insertFile($data);
			if(!$insertFile) throw new Exception("Gagal upload file", 1);

			$resp = [
				'status' => TRUE,
				'message' => 'upload file excel berhasil, klik OK untuk melanjutkan proses upload data dari file excel',
				'id' => $insertFile,
			];
			echo json_encode($resp);
		} catch (Exception $e) {
			$resp = [
				'status' => FALSE,
				'message' => $e->getMessage(),
			];
			echo json_encode($resp);
		}
	}

	function proses_upload()
	{
		$file_id = $this->input->post("file_id");
		$data_array = [
			'WITEL' => "",
			'DATEL' => "",
			'CMDF' => "",
			'ND' => "",
			'ND_REFERENCE' => "",
			'TGL_AKTIF' => "",
			'UMUR' => "",
			'RANGE' => "",
			'DP' => "",
			'KWADRAN' => "",
			'TUNDA_CABUT' => "",
			'BLNTAG_TUNDA_CABUT' => "",
			'CITEM' => "",
			'DESC_ITEM' => "",
			'TAG_INET' => "",
			'TAG_POTS' => "",
			'NOM_RESSOURCE' => "",
			'ONU_STATUS' => "",
			'ABO-INET' => "",
			'PAKET' => "",
			'TYPE_FILE' => $file_id,
			'STATUS' => "kosong",
			'POSISI' => "1",
		];
		$data_insert = json_decode($_POST['myData'],true);
		$data_dunning = array();
		for($a=0;$a<count($data_insert);$a++){
			unset($data_insert[$a]['NO']);

			$date = strtotime($data_insert[$a]['TGL_AKTIF']);
			if($date){
				$time = date("Y-m-d",$date);
				$data_insert[$a]['TGL_AKTIF'] = $time;
			}

			$key = array_keys($data_insert[$a]);
			for($b=0;$b<count($key);$b++){
				if(array_key_exists($key[$b], $data_array)){
					$data_array[$key[$b]] = $data_insert[$a][$key[$b]];
				}
			}
			array_push($data_dunning, $data_array);
			$data_array = [
				'WITEL' => "",
				'DATEL' => "",
				'CMDF' => "",
				'ND' => "",
				'ND_REFERENCE' => "",
				'TGL_AKTIF' => "",
				'UMUR' => "",
				'RANGE' => "",
				'DP' => "",
				'KWADRAN' => "",
				'TUNDA_CABUT' => "",
				'BLNTAG_TUNDA_CABUT' => "",
				'CITEM' => "",
				'DESC_ITEM' => "",
				'TAG_INET' => "",
				'TAG_POTS' => "",
				'NOM_RESSOURCE' => "",
				'ONU_STATUS' => "",
				'ABO-INET' => "",
				'PAKET' => "",
				'TYPE_FILE' => $file_id,
				'STATUS' => "kosong",
				'POSISI' => "1",
			];
			
		}
		$this->db->insert_batch('data_dunning', $data_dunning);
		echo count($data_dunning);
	}

	public function update_file()
	{
		$file_id = $this->input->post('file_id');
		$status = $this->input->post('status');

		$update_file = $this->M_data->updateFile($status,$file_id);
	}
}
?>