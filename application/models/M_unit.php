<?php
class M_unit extends CI_Model
{	
	private $tb_data = "data_dunning";
    private $tb_file = "data_upload";

	var $column_order_data = array('WITEL','DATEL','CMDF','ND','ND_REFERENCE','TGL_AKTIF','UMUR','RANGE','DP','KWADRAN','TUNDA_CABUT','BLNTAG_TUNDA_CABUT','CITEM','DESC_ITEM','TAG_INET','TAG_POTS','NOM_RESSOURCE','ONU_STATUS','ABO-INET','PAKET','TYPE_FILE','STATUS','KETERANGAN');
    var $column_search_data = array('WITEL','DATEL','CMDF','ND','ND_REFERENCE','TGL_AKTIF','UMUR','RANGE','DP','KWADRAN','TUNDA_CABUT','BLNTAG_TUNDA_CABUT','CITEM','DESC_ITEM','TAG_INET','TAG_POTS','NOM_RESSOURCE','ONU_STATUS','ABO-INET','PAKET','TYPE_FILE','STATUS','KETERANGAN');
    var $order_data = array('TYPE_FILE' => 'desc');


	public function updateData($posisi,$keterangan,$nd,$type)
	{
		$data = array(
	        'POSISI' => $posisi,
	        'KETERANGAN' => $keterangan
		);

		$this->db->where('ND', $nd);
		$this->db->where('TYPE_FILE', $type);
		return $this->db->update($this->tb_data, $data);
	}

	public function getWitel()
	{
		$query = $this->db->select("DISTINCT(witel)")
						->from($this->tb_data);
		return $query->get()->result();
	}

	public function getDatel()
	{
		$query = $this->db->select("DISTINCT(datel)")
						->from($this->tb_data);
		return $query->get()->result();
	}

	public function getLastID()
	{
		$query = $this->db->select("id")
						->from($this->tb_file)
						->order_by("id", "desc");
		$query = $query->get()->row();
		if($query){
			return $query->id;
		}else{
			return false;
		}
	}





	/*====================================================================================================*/
	private function _get_datatables_query($type,$data_where = null)
    {
    	$this->db->select("
            WITEL,
            DATEL,
            CMDF,
            ND,
            ND_REFERENCE,
            TGL_AKTIF,
            UMUR,
            RANGE,
            DP,
            KWADRAN,
            TUNDA_CABUT,
            BLNTAG_TUNDA_CABUT,
            CITEM,
            DESC_ITEM,
            TAG_INET,
            TAG_POTS,
            NOM_RESSOURCE,
            ONU_STATUS,
            ABO-INET AS ABO,
            PAKET,
            TYPE_FILE,
            STATUS,
            KETERANGAN,
            deskripsi    AS POSISI
        ");
        $this->db->from($this->tb_data);
        $this->db->join("unit","data_dunning.POSISI = unit.id");
        $this->db->where("TYPE_FILE",$data_where['id']);
        $this->db->where("POSISI",$data_where['posisi']);

        $column_search = $this->column_search_data;
        $column_order = $this->column_order_data;
        $order = $this->order_data;
    	
        $i = 0;
     
        foreach ($column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($type == "file"){
        		$this->db->order_by($column_order[$_REQUEST['order']['0']['column']], /*$_REQUEST['order']['0']['dir']*/"desc");
        	}
        } 
        else
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($type,$data_where = null)
    {
        $this->_get_datatables_query($type,$data_where);
        if($type == "aso"){
        	$this->db->like('WITEL', $data_where['witel']);
			$this->db->like('DATEL', $data_where['datel']);
        }
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($type,$data_where = null)
    {
        $this->_get_datatables_query($type,$data_where);
        if($type == "aso"){
        	$this->db->like('WITEL', $data_where['witel']);
			$this->db->like('DATEL', $data_where['datel']);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($type,$data_where = null)
    {
		$this->db->from($this->tb_data);
		$this->db->where("TYPE_FILE",$data_where['id']);
        $this->db->where("POSISI",$data_where['posisi']);
    	
        return $this->db->count_all_results();
    }
}
?>