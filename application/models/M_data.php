<?php
class M_data extends CI_Model
{	
	private $tb_data = "data_dunning";
    private $tb_file = "data_upload";

    var $column_order_file = array('id','nama_file','upload_at','upload_by','status');
    var $column_search_file = array('id','nama_file','upload_at','upload_by','status');
    var $order_file = array('id' => 'desc');

	function insertFile($data)
	{
		$this->db->insert($this->tb_file, $data);
		return $this->db->insert_id();
	}

	public function insertData($data)
	{
		return $this->db->insert_batch($this->tb_data, $data);
	}

	public function updateFile($status,$id)
	{
		$data = array(
	        'status' => $status,
		);

		$this->db->where('id', $id);
		return $this->db->update($this->tb_file, $data);
	}

	public function getFile()
	{
		$query = $this->db->select("*")
						->from($this->tb_file)
						->order_by("id", "desc");
		return $query->get()->result();
	}

	public function getData($type)
	{
		$query = $this->db->select("*")
						->from($this->tb_data)
						->where("type_file",$type);
		return $query->get()->result();
	}

	public function getCountData($status,$type)
	{
		$this->db->where('type_file', $type);
		$this->db->where('status', $status);
		$num_rows = $this->db->count_all_results($this->tb_data);
		return $num_rows;
	}

    public function getCountDataKategori($status,$type,$posisi)
    {
        $this->db->where('type_file', $type);
        $this->db->where('status', $status);
        $this->db->where('posisi', $posisi);
        $num_rows = $this->db->count_all_results($this->tb_data);
        return $num_rows;
    }

	public function getLastID()
	{
		$query = $this->db->select("id")
						->from($this->tb_file)
						->order_by("id", "desc");
		$query = $query->get()->row();
		if($query){
			return $query->id;
		}else{
			return false;
		}
	}





	/*====================================================================================================*/
	private function _get_datatables_query($type,$data_where = null)
    {
    	$this->db->from($this->tb_file);

        $column_search = $this->column_search_file;
        $column_order = $this->column_order_file;
        $order = $this->order_file;
    	
        $i = 0;
     
        foreach ($column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($type == "file"){
        		$this->db->order_by($column_order[$_REQUEST['order']['0']['column']], /*$_REQUEST['order']['0']['dir']*/"desc");
        	}
        } 
        else
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($type,$data_where = null)
    {
        $this->_get_datatables_query($type,$data_where);
        
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($type,$data_where = null)
    {
        $this->_get_datatables_query($type,$data_where);
        
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($type,$data_where = null)
    {
    	$this->db->from($this->tb_file);
        return $this->db->count_all_results();
    }
}
?>